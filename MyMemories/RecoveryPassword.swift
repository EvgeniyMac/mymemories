//
//  RecoveryPassword.swift
//  MyMemories
//
//  Created by Evgeniy Suprun on 21.11.16.
//  Copyright © 2016 Evgeniy Suprun. All rights reserved.
//

import UIKit
import Parse

class RecoveryPassword: UIViewController {

    @IBOutlet weak var emailTextRecovery: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    
    @IBAction func recoverySendButton(_ sender: Any) {
        
     let userRecoveryEmail = emailTextRecovery.text
        
        if (userRecoveryEmail!.isEmpty) {
        displayAlertMessage(userMessage: "Email text field is Empty!")
            return
        } else
        {
        
        if isValidEmail(userEmail:userRecoveryEmail!) == false {
            
            displayAlertMessage(userMessage: "Not correct e-mail adress you write!")
            return
        }
            // display alert message
            
        let myAlert = UIAlertController(title: "Alert", message: "You password send on your e-mail! Thank you!", preferredStyle: UIAlertControllerStyle.alert)
            
        let okAction = UIAlertAction(title: "Done", style: UIAlertActionStyle.default) {
            action in
        self.performSegue(withIdentifier: "showAfterRecovery", sender: self)
                
            }
            
        myAlert.addAction(okAction)
        self.present(myAlert, animated: true, completion: nil)
            
    }
}

    
    func isValidEmail(userEmail:String) -> Bool {
        
        // print("validate calendar: \(userEmail)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: userEmail)
    }
    
    func displayAlertMessage(userMessage: String) {
        
        let myAlert = UIAlertController(title: "Alert", message: userMessage, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "Done", style: UIAlertActionStyle.default, handler: nil)
        
        myAlert.addAction(okAction)
        self.present(myAlert, animated: true, completion: nil)
        
    }


    @IBAction func cancelRecovery(_ sender: Any) {
    self.dismiss(animated: true, completion: nil)

   }

}
