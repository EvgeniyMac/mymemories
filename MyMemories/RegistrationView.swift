//
//  RegistrationView.swift
//  MyMemories
//
//  Created by Evgeniy Suprun on 21.11.16.
//  Copyright © 2016 Evgeniy Suprun. All rights reserved.
//

import UIKit
import Parse

class RegistrationView: UIViewController{

    @IBOutlet var registEmailText: UITextField!
    
    @IBOutlet var registrPasswordText: UITextField!
    
    @IBOutlet var confPaswordText: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func registerButton(_ sender: Any) {
    
        let userEmail = registEmailText.text
        let userPassword = registrPasswordText.text
        let userRepeatPassword = confPaswordText.text
    

        
        // check email text fields
        
        if isValidEmail(userEmail: userEmail!) == false {
            
           displayAlertMessage(userMessage: "Not correct e-mail adress you write!")
           return
        } else {
        
        if (userEmail!.isEmpty) || (userPassword!.isEmpty) || (userRepeatPassword!.isEmpty) {
            
            displayAlertMessage(userMessage: "Not all textfields are filled!")
            return
        }
        // check password match
        if (userPassword != userRepeatPassword) {
            displayAlertMessage(userMessage: "Password does not match, try again!")
            }
            
            
            // display alert message
            
            let myAlert = UIAlertController(title: "Alert", message: "Registration is sucssesful!", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "Done", style: UIAlertActionStyle.default) {
                action in
                self.performSegue(withIdentifier: "showAfterRegistration", sender: self)
            
                
            }
            
            myAlert.addAction(okAction)
            self.present(myAlert, animated: true, completion: nil)
            
            
        }
        
       
        
    
}
    
    func isValidEmail(userEmail:String) -> Bool {
        
        // print("validate calendar: \(userEmail)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: userEmail)
    }

    
    func displayAlertMessage(userMessage: String) {
        
        let myAlert = UIAlertController(title: "Alert", message: userMessage, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "Done", style: UIAlertActionStyle.default, handler: nil)
        
        myAlert.addAction(okAction)
        self.present(myAlert, animated: true, completion: nil)
        
    }


    @IBAction func registCancelButton(_ sender: Any) {
         self.dismiss(animated: true, completion: nil)
    }
    
}
