//
//  ViewMyAlbum.swift
//  MyMemories
//
//  Created by Evgeniy Suprun on 21.11.16.
//  Copyright © 2016 Evgeniy Suprun. All rights reserved.
//

import UIKit

let reuIndentifire = "PhotoCell"

class ViewMyAlbum: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    


    
  
    @IBAction func photoCameraButton(_ sender: Any) {
        
            if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
                //load the camera interface
                let picker : UIImagePickerController = UIImagePickerController()
                picker.sourceType = UIImagePickerControllerSourceType.camera
                picker.delegate = self
                picker.allowsEditing = false
                self.present(picker, animated: true, completion: nil)
            }else{
                //no camera available
                let alert = UIAlertController(title: "Error", message: "There is no camera available", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: {(alertAction)in
                    alert.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
        }
        
            
}

    
    
    
    @IBAction func chekPhotoButton(_ sender: Any) {

            let picker : UIImagePickerController = UIImagePickerController()
            picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            picker.delegate = self
            picker.allowsEditing = false
            self.present(picker, animated: true, completion: nil)
        }
        

    
    
    @IBAction func photoAlbumLoginButton(_ sender: Any) {
    self.navigationController!.popViewController(animated: true)

    }
    
    
    @IBOutlet var collectionView: UICollectionView! = nil
   

    override func viewDidLoad() {
      self.tabBarController?.delegate = UIApplication.shared.delegate as? UITabBarControllerDelegate
       self.navigationController?.isToolbarHidden = true
       self.navigationController?.isNavigationBarHidden = false
       collectionView.delegate = self
       collectionView.dataSource = self
     
    }
        
       //  Do any additional setup after loading the view.
 
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
             return 1
            }


func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
    let cell: UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: reuIndentifire, for: indexPath as IndexPath) as UICollectionViewCell
    
    cell.backgroundColor = UIColor.red
    
    return cell
}
}
    
       

  
