//
//  ViewPhotoAlbum.swift
//  MyMemories
//
//  Created by Evgeniy Suprun on 21.11.16.
//  Copyright © 2016 Evgeniy Suprun. All rights reserved.
//

import UIKit
import Photos

class ViewPhotoAlbum: UIViewController, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    var assetCollection: PHAssetCollection!
    var photosAsset: PHFetchResult<PHAsset>!
    var index: Int = 0
    
    //@Return to photos
    
    
    
    
    
    @IBAction func sizeCancelButton(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true) //!!Added Optional Chaining
    }
    
    //@Export photo
    
    @IBAction func fullSizeExportButton(_ sender : AnyObject) {
    
        print("Export")
    }
    
    //@Remove photo from Collection
    @IBAction func fullSizeTrashButton(_ sender : AnyObject) {
        let alert = UIAlertController(title: "Delete Image", message: "Are you sure you want to delete this image?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default,
     handler: {(alertAction)in PHPhotoLibrary.shared().performChanges({ //Delete Photo
     
        if let request = PHAssetCollectionChangeRequest(for: self.assetCollection){
        request.removeAssets(self.photosAsset[self.index] as! NSFastEnumeration)
    }
    },
    completionHandler: {(success, error)in
      NSLog("\nDeleted Image -> %@", (success ? "Success":"Error!"))
    alert.dismiss(animated: true, completion: nil)
    
        if(success){ // Move to the main thread to execute
    DispatchQueue.main.async(execute: {
    self.photosAsset = PHAsset.fetchAssets(in: self.assetCollection, options: nil)
    
        if(self.photosAsset.count == 0){
         print("No Images Left!!")
    _ = self.navigationController?.popViewController(animated: true)
    }else
    {
    if(self.index >= self.photosAsset.count){
    self.index = self.photosAsset.count - 1
    }
//        self.displayPhoto()
    }
    })
    }else{
    print("Error: \(error)")
    }
})
}))
        
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: {(alertAction)in
            //Do not delete photo
            alert.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    @IBOutlet var imgView: UIImageView! = nil
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
        self.navigationController?.hidesBarsOnTap = true
       
        
//        self.displayPhoto()
    }
    
        
    
    
//    func displayPhoto(){
//        // Set targetSize of image to iPhone screen size
//        let screenSize: CGSize = UIScreen.main.bounds.size
//        let targetSize = CGSize(width: screenSize.width, height: screenSize.height)
//        
//        let imageManager = PHImageManager.default()
//        if let asset = self.photosAsset[self.index] as PHAsset!{
//            imageManager.requestImage(for: asset, targetSize: targetSize, contentMode: .aspectFit, options: nil, resultHandler: {
//                (result, info)->Void in
//                self.imgView.image = result
//            })
//        }
    }
    
    
    
