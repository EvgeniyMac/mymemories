//
//  ViewController.swift
//  MyMemories
//
//  Created by Evgeniy Suprun on 08.11.16.
//  Copyright © 2016 Evgeniy Suprun. All rights reserved.
//

import UIKit
import LocalAuthentication
import Parse

class LoginViewController: UIViewController
{
    
    @IBOutlet var emailLoginText: UITextField!
    
    @IBOutlet var passwordLoginText: UITextField!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationController?.isToolbarHidden = true
        self.navigationController?.isNavigationBarHidden = true
    }

    
    @IBAction func loginButtonForm(_ sender: Any) {
        
        
        let userLogin = emailLoginText.text
        let userPassword = passwordLoginText.text
        
        
        
        
        // check email text fields
        
        
        
        if (userLogin!.isEmpty) || (userPassword!.isEmpty){
            
            displayAlertMessage(userMessage: "Not all textfields are filled!")
            return
        } else
        {

            if isValidEmail(userEmail: userLogin!) == false {
            
            displayAlertMessage(userMessage: "Not correct e-mail adress you write!")
            return
        }
    
            // display alert message
            
            let myAlert = UIAlertController(title: "Alert", message: "Login is Sucssesful! Welcome my friend :)", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "Done", style: UIAlertActionStyle.default) {
                action in
                self.performSegue(withIdentifier: "afterLoginShow", sender: self)
                
                
            }
            
            myAlert.addAction(okAction)
            self.present(myAlert, animated: true, completion: nil)
            
            
        }

        
    }
    
    @IBAction func IdButton(_ sender: UIButton) { self.authenticateUser() // Do Idfication button
        
 
    }

    
    // MARK: Touch ID Authentication
    
    func authenticateUser()
    {
        let context = LAContext()
        var error: NSError?
        let reasonString = "Authentication is needed to access your Memories!"
        
        if context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: &error)
        {
            context.evaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, localizedReason: reasonString, reply: { (success, policyError) -> Void in
                
                if success
                {
                    self.performSegue(withIdentifier: "afterLoginShow", sender: self)
                    print("Authentication successful!")
                    
                }
                else {
                
                    
                    switch policyError!._code
                    {
                    case LAError.Code.systemCancel.rawValue:
                        print("Authentication was cancelled by the system!")
                    case LAError.Code.userCancel.rawValue:
                        print("Authentication was cancelled by the user!")
                        
                    case LAError.Code.userFallback.rawValue:
                        print("User selected to enter password!")
                        OperationQueue.main.addOperation({ () -> Void in
                              //showPasswordAlert()
                        })
                    default:
                        print("Authentication failed!")
                        OperationQueue.main.addOperation({ () -> Void in
                           // showPasswordAlert()
                        })
                    }
                }
            })
        
        }
        else
        {
            print(error?.localizedDescription as Any)
            OperationQueue.main.addOperation({ () -> Void in
                    //showPasswordAlert()
           })
    }
}

func isValidEmail(userEmail:String) -> Bool {
    
    // print("validate calendar: \(userEmail)")
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: userEmail)
}

func displayAlertMessage(userMessage: String) {
    
    let myAlert = UIAlertController(title: "Alert", message: userMessage, preferredStyle: UIAlertControllerStyle.alert)
    let okAction = UIAlertAction(title: "Done", style: UIAlertActionStyle.default, handler: nil)
    
    myAlert.addAction(okAction)
    self.present(myAlert, animated: true, completion: nil)
    
}

}

//    // MARK: Password Alert
//
//    func showPasswordAlert()
//        {
//        let alertController = UIAlertController(title: "Touch ID Password", message: "Пожайлуста введите Ваш пароль!", preferredStyle: .alert)
//        
//        let defaultAction = UIAlertAction(title: "OK", style: .cancel) { (action) -> Void in
//            
//            if let textField = alertController.textFields?.first as UITextField?
//            {
//                if textField.text == "123"
//                {
//                    print("Authentication successful!")
//                }
//                else
//                {
//                    showPasswordAlert()
//                }
//            }
//        }
//        
//        alertController.addAction(defaultAction)
//       
//        
//        
//        alertController.addTextField { (textField) -> Void in
//            
//            textField.placeholder = "Password"
//            textField.isSecureTextEntry = true
//            
//        }
//        //  present(alertController, animated: true, completion: nil)



